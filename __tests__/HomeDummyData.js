export const data = {
  count: 1118,
  next: 'https://pokeapi.co/api/v2/pokemon?offset=110&limit=10',
  previous: 'https://pokeapi.co/api/v2/pokemon?offset=90&limit=10',
  results: [
    {
      name: 'electrode',
      url: 'https://pokeapi.co/api/v2/pokemon/101/',
    },
    {
      name: 'electrode',
      url: 'https://pokeapi.co/api/v2/pokemon/102/',
    },
    {
      name: 'electrode',
      url: 'https://pokeapi.co/api/v2/pokemon/103/',
    },
    {
      name: 'electrode',
      url: 'https://pokeapi.co/api/v2/pokemon/104/',
    },
  ],
};
